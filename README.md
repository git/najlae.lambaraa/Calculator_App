# Captures d'écrans de la vue

<img src="./Documentation/images/cap1.png" width=200/>
<img src="./Documentation/images/cap2.png" width=200/>
<img src="./Documentation/images/cap3.png" width=200/>
<img src="./Documentation/images/cap5.png" width=200/>
<img src="./Documentation/images/cap7.png" width=200/>

## Le travail rendu

- Ajouter des Matières
- Enlever des Matières
- Modifier des Matières
- Changer le nom
- Changer le coefficient
- La moyenne des Matières
- Total de projet/stage
